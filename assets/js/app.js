var $ = require('jquery');
require('jquery-ui-bundle');
require('jquery-ui-bundle/jquery-ui.css');
require('../css/app.scss');

$.datepicker.setDefaults( $.datepicker.regional[ "pl" ] );

$(document).ready(function() {
    $('.datepicker').datepicker(
        {
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            maxDate: new Date,
        }
        );
});
