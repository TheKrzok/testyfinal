<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 19.01.2018
 * Time: 11:36
 */

namespace App\Application\SecurityModule\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Klasa budująca formularz do zmiany hasła, gdy te wygaśnie
 *
 * Class PasswordExpiredType
 * @package App\Form\SecurityModule
 */
class PasswordExpiredType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Nazwa użytkownika',
                'constraints' => array(
                    new NotBlank(array('message' => 'Nazwa użytkownika nie może być pusta'))
                )
            ))
            ->add('oldPassword', PasswordType::class, array(
                'label' => 'Stare hasło',
                'constraints' => array(
                    new NotBlank(array('message' => 'Podaj stare hasło'))
                )
            ))
            ->add('newPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array(
                    'label' => 'Nowe hasło',
                    'constraints' => array(
                        new NotBlank(array('message' => 'Nowe hasło nie może być puste')),
                        new Length(array(
                            'min' => 6, 'minMessage' => 'Nowe hasło nie może mieć mniej niż 6 znaków',
                            'max' => 20, 'maxMessage' => 'Nowe hasło nie może mieć więcej niż 20 znaków'
                        ))
                    )
                ),
                'second_options' => array('label' => 'Powtórz nowe'),
                'invalid_message' => 'Podane wartości różnią się'
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zmień hasło',
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
            ;
    }
}