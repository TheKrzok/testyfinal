<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 13.01.2018
 * Time: 19:17
 */

namespace App\Application\SecurityModule\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Klasa budująca formularz rejestracyjny
 *
 * Class RegisterType
 * @package App\Form\SecurityModule
 */
class RegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Imię',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Imię nie może być puste')),
                    new Length(array(
                        'min' => 3, 'minMessage' => 'Imię nie może być krótsze niż 2 znaki',
                        'max' => 30, 'maxMessage' => 'iImię nie może być dłuższe niż 50 znaków'))
                )
            ))
            ->add('surname', TextType::class, array(
                'label' => 'Nazwisko',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Nazwisko nie może być puste')),
                    new Length(array(
                        'min' => 3, 'minMessage' => 'Nazwisko nie może być krótsze niż 2 znaki',
                        'max' => 50, 'maxMessage' => 'Nazwisko nie może być dłuższe niż 50 znaków')),
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Adres e-mail',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Adres E-mail nie może być pusty')),
                    new Email(array('message' => 'Podana wartość nie jest poprawnym adresem e-mail')),
                    new Length(array(
                        'min' => 4, 'minMessage' => 'E-mail nie może być krótszy niż 4 znaki',
                        'max' => 50, 'maxMessage' => 'E-mail nie może być dłuższy niż 50 znaków'
                    ))
                )
            ))
            ->add('pesel', TextType::class, array(
                'label' => 'Numer pesel',
                'constraints' => array(
                    new NotBlank(array('message' => 'Pesel nie może być pusty')),
                    new Length(array(
                        'min' => 11,
                        'max' => 11,
                        'exactMessage' => 'Numer pesel musi mieć dokładnie 11 znaków'
                    )),
                    new Regex(array(
                        'pattern' => '/[0-9]{11}/',
                        'match' => true,
                        'message' => 'Numer pesel może składać się wyłącznie z liczb'
                    ))
                )
            ))
            ->add('birthdate', TextType::class, array(
                'label' => 'Data urodzenia',
                'attr' => array('class' => 'datepicker', 'readonly' => 'readonly'),
                'mapped' => false,
                'constraints' => array(
                    new NotBlank(array('message' => 'Data urodzenia nie może być pusta'))),
            ))
            ->add('username', TextType::class, array(
                'label' => 'Nazwa użytkownika',
                'trim' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Nazwa użytkownika nie może być pusta')),
                    new Length(array(
                        'min' => 3, 'minMessage' => 'Nazwa uzytkownika nie może być krótsza niż 3 znaki',
                        'max' => 20, 'maxMessage' => 'Nazwa użytkownika nie może być dłuższa niż 20 znaków'
                    )),
                )
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array(
                    'label' => 'Hasło',
                    'constraints' => array(
                        new NotBlank(array('message' => 'Hasło nie może być puste')),
                        new Length(array(
                            'min' => 6, 'minMessage' => 'Hasło nie może mieć mniej niż 6 znaków',
                            'max' => 20, 'maxMessage' => 'Hasło nie może mieć więcej niż 20 znaków'
                        ))
                    )
                ),
                'second_options' => array('label' => 'Powtórz hasło'),
                'invalid_message' => 'Podane wartości różnią się'
            ))
            ->add('file1', FileType::class, array(
                'mapped' => false,
                'label' => 'Zdjęcie przodu dowodu osobistego',
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Zdjęcie przodu dowodu jest obowiązkowe'
                    )),
                    new Image(array(
                        'maxSize' => '2M',
                        'maxSizeMessage' => "Zdjęcie nie może być większe niż 2 megabajty",
                        'mimeTypes' => array('image/jpg', 'image/jpeg'),
                        'mimeTypesMessage' => 'Zdjęcie musi być w formacie jpg lub jpeg'
                    )),
                )
            ))
            ->add('file2', FileType::class, array(
                'mapped' => false,
                'label' => 'Zdjęcie tyłu dowodu osobistego',
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Zdjęcie tyłu dowodu jest obowiązkowe'
                    )),
                    new Image(array(
                        'maxSize' => '2M',
                        'maxSizeMessage' => "Zdjęcie nie może być większe niż 2 megabajty",
                        'mimeTypes' => array('image/jpg', 'image/jpeg'),
                        'mimeTypesMessage' => 'Zdjęcie musi być w formacie jpg lub jpeg'
                    )),
                )
            ))
            ->add('submit', SubmitType::class, array('label' => 'Zarejestruj się', 'attr' => array('class' => 'btn btn-success')))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}