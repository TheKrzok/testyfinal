<?php

namespace App\Application\SecurityModule\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 18.01.2018
 * Time: 12:06
 */

/**
 * Klasa budująca formularz do zmiany hasła gdy użytkownik zapomni hasła
 *
 * Class PasswordChangeType
 * @package App\Form\SecurityModule
 */
class PasswordChangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plainPassword', RepeatedType::class, array
            (
                'type' => PasswordType::class,
                'first_options'  => array(
                    'label' => 'Nowe hasło',
                    'constraints' => array(
                        new NotBlank(array('message' => 'Nowe hasło nie może być puste')),
                        )
                ),
                'second_options' => array(
                    'label' => 'Powtórz hasło',
                    'constraints' => array(new NotBlank(array('message' => 'Nowe hasło nie może być puste')),
            ))
            ))->add('submit', SubmitType::class, array('label' => 'Zmień hasło', 'attr' => array('class' => 'btn btn-success')));
    }
}