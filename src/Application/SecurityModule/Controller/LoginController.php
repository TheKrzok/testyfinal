<?php
namespace App\Application\SecurityModule\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Kontroler akcji zalogowania użytkownika
 *
 * Class LoginController
 * @package App\Controller\SecurityModule
 */
class LoginController extends AbstractController
{
    /**
     * Akcja zalogowania użytkownika
     *
     * @Route("security/login", name="login")
     * @param AuthenticationUtils $authUtils
     * @return Response
     */
    public function loginAction(AuthenticationUtils $authUtils)
    {
        $error          = $authUtils->getLastAuthenticationError();
        $lastUsername   = $authUtils->getLastUsername();

        return $this->render('@SecurityModule/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
}