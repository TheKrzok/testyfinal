<?php

namespace App\Application\SecurityModule\Controller;

use App\Application\SecurityModule\Form\RegisterType;
use App\Entity\User;
use App\Service\DateService;
use App\Service\FileUploaderService;
use App\Service\MailerService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * Kontroler akcji rejestracji w serwisie
 *
 * Class RegisterController
 * @package App\Controller\SecurityModule
 */
class RegisterController extends AbstractController
{
    /**
     * Akcja rejestracji w serwisie
     *
     * @Route("security/register", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailerService $mailer
     * @param DateService $dateService
     * @param FileUploaderService $uploader
     * @return RedirectResponse|Response
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, MailerService $mailer, DateService $dateService, FileUploaderService $uploader)
    {
        $user = new User();

        $form = $this->createForm(RegisterType ::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $password   = $passwordEncoder->encodePassword($user, $user->getPlainPassword());

            $birthdateDate  = DateTime::createFromFormat('Y-m-d', $form->get('birthdate')->getData());

            $passwordExpiryDate = $dateService->getNowPlusMonth();

            $file1 = $form->get('file1')->getData();
            $file2 = $form->get('file2')->getData();

            $file1Upload = $uploader->upload($file1);
            $file2Upload = $uploader->upload($file2);

            $user->addFile('przód_dowodu', $file1Upload);
            $user->addFile('tył_dowodu', $file2Upload);
            $user->setPassword($password);
            $user->setBirthdate($birthdateDate);
            $user->setPasswordExpiryDate($passwordExpiryDate);
            $user->addRole('ROLE_USER');
            $user->setIsActive(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Rejestracja przebiegła pomyślnie, na adres e-mail '.$user->getEmail().' została wysłana wiadomość powitalna. Możesz już się zalogować w serwisie'
            );

            $mailer->sendMail('Witaj w systemie!', $user->getEmail(), $this->renderView('@SecurityModule/hello_mail.html.twig', array('name' => $user->getUsername())));

            return $this->redirectToRoute('login');
        }

        return $this->render('@SecurityModule/register.html.twig', array('form' => $form->createView()));
    }
}