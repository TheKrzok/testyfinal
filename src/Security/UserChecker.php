<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 19.01.2018
 * Time: 11:13
 */

namespace App\Security;

use App\Entity\User as AppUser;
use DateTime;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Klasa odpowiadająca za sprawdzenie ważności hasła użytkownika
 * po akcji zalogowania się do systemu
 *
 * Class UserChecker
 * @package App\Security
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param UserInterface $user
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }
    }

    /**
     * @param UserInterface $user
     */
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        $nowDate = new DateTime();
        $expiryDate = $user->getPasswordExpiryDate();

        if($nowDate > $expiryDate) {
            throw new CustomUserMessageAuthenticationException("Ważność hasła wygasła, aby stworzyć nowe hasło skożystaj z opcji 'Hasło wygasło'");
        }
    }
}