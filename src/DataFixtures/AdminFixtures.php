<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 15.01.2018
 * Time: 02:16
 */

/**
 * Fixtura do tworzenia konta administracyjnego
 *
 * Class AdminFixtures
 * @package App\DataFixtures
 */
class AdminFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $encoder = new BCryptPasswordEncoder("8");
        $admin->setName('Rufi');
        $admin->setSurname('Mistrz');
        $admin->setBirthdate(new \DateTime());
        $password = $encoder->encodePassword('admin', 'salt');
        $admin->setPassword($password);
        $admin->addRole('ROLE_ADMIN');
        $admin->setEmail('admin@system.com');
        $admin->setPesel('0000000000000');
        $admin->setUsername('admin');
        $admin->setIsActive(true);

        $manager->persist($admin);
        $manager->flush();
    }
}